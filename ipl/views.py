from django.shortcuts import render
from .models import Matches, Deliveries
from django.http import JsonResponse
from django.db.models import Sum, Count, FloatField
from django.db.models.functions import Cast
import json
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page

#for REST api
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from ipl.models import Matches
from ipl.serializers import MatchSerializer, DeliverySerializer

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

@cache_page(CACHE_TTL)
def home(request):
    return render(request, 'ipl/home.html')


@cache_page(CACHE_TTL)
def matches_played_per_year(request):
    matches_played_per_year = Matches.objects.values('season').order_by('season').annotate(total_matches = Count('id'))
    return JsonResponse(list(matches_played_per_year),safe=False)

@cache_page(CACHE_TTL)
def matches_win_by_team(request):
    matches_win_by_team = Matches.objects.values('winner','season').exclude(result= 'no result').annotate(Count('winner')).order_by('season')
    return JsonResponse(list(matches_win_by_team),safe=False)

@cache_page(CACHE_TTL)
def extra_runs_per_team(request):
    extra_runs_per_team =  Deliveries.objects.filter(match_id__in = Matches.objects.filter(season = '2016')).values('bowling_team').annotate(Sum('extra_runs'))
    return JsonResponse(list(extra_runs_per_team),safe=False)

@cache_page(CACHE_TTL)
def economy_in_2015(request):
    economy_in_2015 = Deliveries.objects.filter(match_id__in = Matches.objects.filter(season = '2015')).values('bowler').annotate(eco =Cast((Sum('total_runs')-Sum('bye_runs'))*6/Count('extra_runs'),  output_field = FloatField())).order_by('eco')
    return JsonResponse(list(economy_in_2015),safe=False)

@cache_page(CACHE_TTL)
def most_mom_awards(request):
    most_mom_awards =  Matches.objects.values('player_of_match').annotate(mom = Count('id')).order_by('-mom')
    return JsonResponse(list(most_mom_awards),safe=False)



@cache_page(CACHE_TTL)
def matches_played(request):
    data = Matches.objects.values('season').order_by('season').annotate(total_matches = Count('id'))
    vals = json.dumps({'data' :  list(data)})
    return render(request, 'ipl/matches.html', context = {"dt":vals})

@cache_page(CACHE_TTL)
def matches_win(request):
    data = Matches.objects.values('winner','season').exclude(result= 'no result').annotate(Count('winner')).order_by('season')
    vals = json.dumps({'data' :  list(data)})
    return render(request, 'ipl/matches_win.html', context = {"dt":vals})

@cache_page(CACHE_TTL)
def extra_runs(request):
    data = Deliveries.objects.filter(match_id__in = Matches.objects.filter(season = '2016')).values('bowling_team').annotate(sum = Sum('extra_runs')).order_by('sum')
    vals = json.dumps({ 'data': list(data)})
    return render(request, 'ipl/extra_runs.html', context = {"dt": vals})

@cache_page(CACHE_TTL)
def economy_2015(request):
    data = Deliveries.objects.filter(match_id__in = Matches.objects.filter(season = '2015')).values('bowler').annotate(eco =Cast((Sum('total_runs')-Sum('bye_runs'))*6/Count('extra_runs'),  output_field = FloatField())).order_by('eco').filter(eco__lte = 7)
    vals = json.dumps({ 'data': list(data)})
    return render(request, 'ipl/economy_2015.html', context = {"dt": vals})

@cache_page(CACHE_TTL)
def mom(request):
    data = Matches.objects.values('player_of_match').annotate(mom = Count('id')).filter(mom__gte = 10).order_by('-mom')
    vals = json.dumps({ 'data': list(data)})
    return render(request, 'ipl/mom.html', context = {"dt": vals})


@csrf_exempt
def matches_list(request):
    """
    List all code matches, or create a new snippet.
    """
    if request.method == 'GET':
        matches = Matches.objects.all()
        serializer = MatchSerializer(matches, many=True)
        return JsonResponse(serializer.data, safe=False)

    # elif request.method == 'POST':
    #     data = JSONParser().parse(request)
    #     serializer = SnippetSerializer(data=data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return JsonResponse(serializer.data, status=201)
    #     return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def match_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        match = Matches.objects.get(pk=pk)
    except Matches.DoeNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = MatchSerializer(match)
        return JsonResponse(serializer.data)

    # elif request.method == 'PUT':
    #     data = JSONParser().parse(request)
    #     serializer = SnippetSerializer(snippet, data=data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return JsonResponse(serializer.data)
    #     return JsonResponse(serializer.errors, status=400)

    # elif request.method == 'DELETE':
    #     snippet.delete()
    #     return HttpResponse(status=204)

@csrf_exempt
def deliveries_list(request):
    """
    List all code deliveries, or create a new snippet.
    """
    if request.method == 'GET':
        deliveries = Deliveries.objects.all()
        serializer = DeliverySerializer(deliveries, many=True)
        return JsonResponse(serializer.data, safe=False)

    # elif request.method == 'POST':
    #     data = JSONParser().parse(request)
    #     serializer = SnippetSerializer(data=data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return JsonResponse(serializer.data, status=201)
    #     return JsonResponse(serializer.errors, status=400)

@csrf_exempt
def delivery_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        delivery = Deliveries.objects.get(pk=pk)
    except deliveryes.DoeNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = DeliverySerializer(delivery)
        return JsonResponse(serializer.data)

    # elif request.method == 'PUT':
    #     data = JSONParser().parse(request)
    #     serializer = SnippetSerializer(snippet, data=data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return JsonResponse(serializer.data)
    #     return JsonResponse(serializer.errors, status=400)

    # elif request.method == 'DELETE':
    #     snippet.delete()
    #     return HttpResponse(status=204)