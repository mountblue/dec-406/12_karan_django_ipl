from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name = 'home'),
    path('1/', views.matches_played_per_year, name = 'matches-wins'),
    path('2/', views.matches_win_by_team, name = 'matches-per-year'),
    path('3/', views.extra_runs_per_team, name = 'matches-extra-runs'),
    path('4/', views.economy_in_2015, name = 'matches-economy'),
    path('5/', views.most_mom_awards, name = 'matches-mom'),
    path('matches_per_year/', views.matches_played, name="matches-played"),
    path('extra_runs_in_2016/', views.extra_runs, name = "extra-runs"),
    path('economy_in_2015/', views.economy_2015, name = "economy"),
    path('mom/', views.mom, name = "mom"),
    path('matches_win/', views.matches_win, name = "matches-win"),
    path('matches/', views.matches_list),
    path('matches/<int:pk>/', views.match_detail),
    path('deliveries/', views.deliveries_list),
    path('deliveries/<int:pk>/', views.delivery_detail),
]
