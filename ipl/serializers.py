from rest_framework import serializers
from ipl.models import Matches, Deliveries

class MatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Matches
        fields = ['season', 'team1', 'team2', 'winner', 'player_of_match']

class DeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = Deliveries
        fields = ['match', 'batting_team', 'bowling_team', 'batsman', 'bowler']


