import csv
from itertools import islice
from django.db import transaction

from django.core.management import BaseCommand, CommandError
from ipl.models import Deliveries, Matches

class Command(BaseCommand):
    help = 'Load data from csv to database'

    def add_arguments(self,parser):
        parser.add_argument('--path', type=str, nargs='+')

    def handle(self, *args, **options):
        matches_array = []
        deliveries_array =[]
        path = options['path']
        for i in range(len(path)):
            if path[i].find('match')==0:
                with open(path[i],'r') as matches:
                    match_reader = csv.DictReader(matches)
                    for match in match_reader:
                        m = Matches()
                        m.id = match['id']
                        m.season = match['season']
                        m.city = match['city']
                        m.date = match['date']
                        m.team1 = match['team1']
                        m.team2 = match['team2']
                        m.toss_winner = match['toss_winner']
                        m.toss_decision = match['season']
                        m.result = match['result']
                        m.dl_applied = match['dl_applied']
                        m.winner = match['winner']
                        m.win_by_runs = match['win_by_runs']
                        m.win_by_wickets = match['win_by_wickets']
                        m.player_of_match = match['player_of_match']
                        m.venue = match['venue']
                        m.umpire1 = match['umpire1']
                        m.umpire2 = match['umpire2']
                        m.umpire3 = match['umpire3']
                        matches_array.append(m)
                    Matches.objects.bulk_create(matches_array)                
            else:
                with open(path[i],'r') as deliveries:
                    deliveries_reader = csv.DictReader(deliveries)
                    for delivery in deliveries_reader:
                        d = Deliveries()
                        d.match_id = delivery['match_id']
                        d.inning = delivery['inning']
                        d.batting_team = delivery['batting_team']
                        d.bowling_team = delivery['bowling_team']
                        d.over = delivery['over']
                        d.ball = delivery['ball']
                        d.batsman = delivery['batsman']
                        d.non_striker = delivery['non_striker']
                        d.bowler = delivery['bowler']
                        d.is_super_over = delivery['is_super_over']
                        d.wide_runs = delivery['wide_runs']
                        d.bye_runs = delivery['bye_runs']
                        d.legbye_runs = delivery['legbye_runs']
                        d.noball_runs = delivery['noball_runs']
                        d.penalty_runs = delivery['penalty_runs']
                        d.batsman_runs = delivery['batsman_runs']
                        d.extra_runs = delivery['extra_runs']
                        d.total_runs = delivery['total_runs']
                        d.player_dismissed = delivery['player_dismissed']
                        d.dismissal_kind = delivery['dismissal_kind']
                        d.fielder = delivery['fielder']
                        deliveries_array.append(d)
                    # Deliveries.objects.bulk_create(deliveries_array)
                    self.my_batch_load(Deliveries, deliveries_array)
    
    def my_batch_load(self, obj, objs):
        batch_size = 2000
        batch_start = 0
        batch_end = batch_size
        while True:
            batch = list(islice(objs, batch_start, batch_end))
            batch_start = batch_end
            batch_end = batch_end + batch_size
            if not batch:
                break
            with transaction.atomic():
                obj.objects.bulk_create(batch, batch_size)