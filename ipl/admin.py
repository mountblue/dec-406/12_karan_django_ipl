from django.contrib import admin
from .models import Deliveries, Matches

admin.site.register(Deliveries)
admin.site.register(Matches)
